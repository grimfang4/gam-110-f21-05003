﻿using System;

namespace RecursionReview
{
    class Program
    {
        // Write a recursive function that counts from 0 to 20, skipping by n numbers each time,
        // where n is an integer that is provided to the function.

        static void PrintSkip(int startingPoint, int n)
        {
            if (startingPoint > 20)
                return;

            Console.WriteLine(startingPoint);
            PrintSkip(startingPoint + n, n);
        }

        static void PrintSkip(int n)
        {
            PrintSkip(0, n);
        }



        /*
         1) Recursive call
         2) Change of input / work towards the base case
         3) Termination condition / base case
         */


        /*IsPalindrome
        Given a string, return a boolean (true or false) indicating if the string contains a palindrome.
        */

        static bool IsPalindrome(string s)
        {
            if (s.Length <= 1)
                return true;

            if (s[0] != s[s.Length - 1])
                return false;

            return IsPalindrome(s.Substring(1, s.Length-2));
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            PrintSkip(3);

            Console.WriteLine("racecar: " + IsPalindrome("racecar"));
            Console.WriteLine("racecars: " + IsPalindrome("racecars"));
            Console.WriteLine("racedcars: " + IsPalindrome("racedcar"));

            Console.ReadLine();
        }
    }
}
