﻿using System;
using System.Collections.Generic;

// Given a string, print out the frequencies of each letter (case-insensitive).

/* Tools:
 * loops
 * strings, and the fact that i can access each letter like an array
 * tolower/toupper
 * if statement, ==
 * console.writeline
 * console.readline
*/

namespace QuinnProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hey there, I'm the string sorter! Give me a string, and I'll tell you the frequency of each letter!");
            string input = Console.ReadLine();
            input = input.ToLower();
            Dictionary<char, int> letterCount = new Dictionary<char, int>();
            

            for (int i = 0; i < input.Length; ++i)
            {
                if (!letterCount.ContainsKey(input[i]))
                {
                    letterCount.Add(input[i], 1);
                }
                else
                {
                    ++letterCount[input[i]];
                }
            }

            for (char c = 'a'; c <= 'z'; ++c)
            {
                if (letterCount.ContainsKey(c))
                {
                    Console.WriteLine(c + ": " + letterCount[c]);
                }
            }
        }
    }
}
