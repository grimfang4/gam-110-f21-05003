﻿using System;
using System.Collections.Generic;

namespace Inheritance_and_Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Animal myAnimal = new Snake();

            Dog gerald = new Dog();

            Dog atraxisTheDestroyerOfWorlds = new Dog();
            atraxisTheDestroyerOfWorlds.SetNumLegs(12);

            myAnimal.Walk();

            gerald.Walk();

            atraxisTheDestroyerOfWorlds.Bark();

            gerald.Vocalize();
            myAnimal.Vocalize();

            // Reference types
            Dog goodBoy = gerald;

            goodBoy.Bark();


            // POLYMORPHISM
            Animal heyYou = gerald;
            heyYou.Vocalize();

            Animal someAnimal = new Snake();
            someAnimal.Vocalize();


            List<Animal> animals = new List<Animal>();

            animals.Add(gerald);
            animals.Add(someAnimal);
            animals.Add(atraxisTheDestroyerOfWorlds);
            animals.Add(new Snake());

            Console.WriteLine("COMMENCE ANIMAL SYMPHONY");
            for(int i = 0; i < animals.Count; ++i)
            {
                animals[i].Vocalize();
            }



            Console.ReadLine();

        }
    }
}
