﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritance_and_Polymorphism
{
    abstract class Animal
    {
        protected int numLegs;
        protected bool hasEyes;

        public Animal()
        {
            numLegs = 4;
            hasEyes = true;
        }

        public int GetNumLegs()
        {
            return numLegs;
        }

        public virtual void SetNumLegs(int n)
        {
            numLegs = n;
        }

        public bool HasEyes()
        {
            return hasEyes;
        }

        public void Walk()
        {
            Console.WriteLine("::walks::");
        }

        public abstract void Vocalize();
    }
}
