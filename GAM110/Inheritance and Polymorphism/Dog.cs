﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritance_and_Polymorphism
{
    class Dog : Animal
    {
        public int cuteness;

        public Dog()
        {
            numLegs = 4;
            cuteness = 100 * numLegs;
        }

        public override void SetNumLegs(int n)
        {
            base.SetNumLegs(n);
            cuteness = 100 * numLegs;
        }

        public override void Vocalize()
        {
            Bark();
        }

        public void Bark()
        {
            if (cuteness > 1000)
                Console.WriteLine("Yip");
            else
                Console.WriteLine("WOOF");
        }
    }
}
