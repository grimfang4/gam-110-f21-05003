﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritance_and_Polymorphism
{
    class Snake : Animal
    {
        public int solidness = 0;

        public Snake()
        {

        }

        public override void Vocalize()
        {
            Console.WriteLine("Hiss!");
        }

        public void LobGrenade()
        {
            Console.WriteLine("A bomb has been planted.");
        }
    }
}
