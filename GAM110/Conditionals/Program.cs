﻿using System;

class Conditionals
{
    public static void Main(string[] args)
    {
        int a;
        a = 5+7;

        string name = "Jon";

        if(a < 20)
        {
            Console.WriteLine("Hey, that's less than 20!");
        }

        if(a >= 50)
        {
            Console.WriteLine("Wow, that's a large number!");
        }
        else
        {
            Console.WriteLine("Okay, that number is not greater than or equal to 50...");
        }

        if(name == "Jon")
        {
            // TODO: Say pleasant things
        }

        if(name != "Jon")
        {
            // TODO: Say unpleasant things
        }

        if(!(name == "Jon"))
        {
            // Same as not-equal Jon
        }

        Console.ReadLine();
    }
}