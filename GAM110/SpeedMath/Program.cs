﻿using System;
using System.Diagnostics;

/*
 * Make a program where the user answers a bunch of math problems and the time it takes is measured.
 * 
 * Timer that starts at start of test, ends at end of test (not tied to program lifetime!)
 * Loop for guessing each answer
 * Counter for correct/incorrect answers
 */

namespace SpeedMath
{
    class Program
    {
        static int PromptInt(string message, string tryAgainMessage)
        {
            int result = 0;
            bool beans = true;
            while(beans)
            {
                Console.WriteLine(message);
                string intAnswer = Console.ReadLine();
                beans = !int.TryParse(intAnswer, out result);
                if (beans)
                    Console.WriteLine(tryAgainMessage);
            }
            return result;
        }

        static int GetQuestion(out string question)
        {
            int answer = 0;
            question = "THIS SHOULD NEVER HAPPEN";

            Random rng = new Random();
            int first = rng.Next(0, 10);
            int second = rng.Next(1, 10);

            int operatorRand = rng.Next(0, 4);

            if (operatorRand == 0)
            {
                // addition
                answer = first + second;
                question = first + " + " + second;
            }
            else if (operatorRand == 1)
            {
                // subtraction
                answer = first - second;
                question = first + " - " + second;
            }
            else if (operatorRand == 2)
            {
                // multiplication
                answer = first * second;
                question = first + " * " + second;
            }
            else if (operatorRand == 3)
            {
                // division
                answer = first / second;
                question = first + " / " + second;
            }

            return answer;
        }

        static void Main(string[] args)
        {
            int numQuestions = 10;
            int correct = 0;
            int incorrect = 0;

            Stopwatch sw = new Stopwatch();

            sw.Start();

            for(int i = 0; i < numQuestions; ++i)
            {
                int answer = 0;
                string question = string.Empty;

                answer = GetQuestion(out question);

                int guess = PromptInt(question + " = ", "That is not a number!!!!");

                if (guess == answer)
                    correct++;
                else
                    incorrect++;
            }

            sw.Stop();

            Console.WriteLine("That took: " + sw.Elapsed);
            Console.WriteLine("You got " + correct + " out of " + (correct + incorrect) + " right.");

            Console.ReadLine();
        }
    }
}
