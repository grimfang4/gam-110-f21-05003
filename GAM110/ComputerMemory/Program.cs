﻿using System;

namespace ComputerMemory
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            // 01110101 11000110 11001000 00000100
            //int - 4 bytes, 32 bits
            //uint - 4 bytes (unsigned - no negative values)

            //float - 4 bytes
            float myFloat = 7.45678956765f;

            //long - 8 bytes
            //short

            //double - 8 bytes

            //bool - 1 bit?
            Console.WriteLine("Size of an integer in BYTES: " + sizeof(int));
            Console.WriteLine("Range of an integer: " + int.MinValue + " / " + int.MaxValue);
            Console.WriteLine("Size of a boolean in BYTES: " + sizeof(bool));

            Console.ReadLine();

            //char
            char c = 'y';

            //array
            //wide?
            //half
            //string


            
            int gradeForMatt = 95;
            int gradeForLiam = 75;

            

            // Calculate the average grade

            float average = (gradeForMatt + gradeForLiam) / 2.0f;


            // Let's use an array instead!

            //int[] grades = { 95, 75, 60, 80, 90, 97, 78, 34, 99, 100, 87, 96};
            int[] grades = new int[100];

            for(int i = 0; i < 100; ++i)
            {
                grades[i] = i;
            }


            int sum = 0;
            for(int i = 0; i < grades.Length; ++i)
            {
                sum += grades[i];
            }

            float average2 = sum / (float)grades.Length;

            Console.WriteLine("Average2: " + average2);



        }
    }
}
