﻿using System;

namespace Recursion
{
    class Program
    {

        static float Square(float number)
        {
            return number * number;
        }

        static float Distance(float x1, float y1, float x2, float y2)
        {
            return (float)Math.Sqrt(Square(x2 - x1) + Square(y2 - y1));
        }

        /*
         * 1) Recursive call
         * 2) Input change (work towards the base case)
         * 3) Termination condition (base case)
         */
        static void Countdown(int start)
        {
            if (start < 0)
                return;
            Console.WriteLine(start);
            Countdown(start - 1);
        }

        static string GetAlphabet(char c)
        {
            if(c == 'z')
            {
                return c.ToString();
            }
            string s = string.Empty;
            s += c;
            return s + GetAlphabet((char)(c+1));
        }

        static string GetAlphabet()
        {
            return GetAlphabet('a');
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string alphabet = GetAlphabet();
            Console.WriteLine(alphabet);

            Countdown(5);

            Console.ReadLine();
        }
    }
}
