﻿using System;
using System.Collections.Generic;

namespace GenericContainers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] array = {44, 2, 6, 8, 31};

            List<int> myList = new List<int>();

            myList.Add(7);
            myList.Add(3);

            for(int i = 0; i < myList.Count; ++i)
            {
                Console.WriteLine("List element " + i + ": " + myList[i]);
            }

            // Warning: Generates garbage, but otherwise very useful.
            foreach(int n in myList)
            {
                Console.WriteLine("List element: " + n);
            }

            List<string> ls = new List<string>();
            ls.Insert(0, "First string");
            ls.Insert(1, "Last string");
            ls.Insert(1, "Middle string");



            Dictionary<string, string> myDictionary = new Dictionary<string, string>();

            myDictionary.Add("Beans", "Bouncy legumes");
            myDictionary.Add("Lentils", "Something else legumes");

            Console.WriteLine(myDictionary["Beans"]);

            Console.WriteLine("Write a sentence:");
            string input = Console.ReadLine();

            string[] words = input.Split(' ');
            for(int i = 0; i < words.Length; ++i)
            {
                string word = words[i];
                //if (myDictionary.ContainsKey(word))
                //    word = myDictionary[word];
                if (myDictionary.TryGetValue(word, out string result))
                    word = result;

                Console.Write(word + " ");
            }


            Console.ReadLine();
        }
    }
}
