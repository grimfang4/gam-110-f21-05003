﻿using System;

namespace MazeGamePlanning
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[,] gridData = new int[15, 10]; // Make a 15x10 block of integers
            gridData[3, 2] = 17; // Access the 2D array
            Console.WriteLine("Dimensions: " + gridData.GetLength(0) + " x " + gridData.GetLength(1));


            char[,] mazeData = {{'#', '#', '#', '#', '#', '#'},
                                {'#', ' ', ' ', ' ', ' ', '#'},
                                {'#', ' ', '#', '#', ' ', '#'},
                                {'#', ' ', '#', ' ', ' ', '#'},
                                {'#', ' ', '#', '#', ' ', '#'},
                                {'#', 'S', '#', 'F', ' ', '#'},
                                {'#', '#', '#', '#', '#', '#'} };
            Console.WriteLine("Dimensions: " + mazeData.GetLength(0) + " x " + mazeData.GetLength(1));
            // 7 x 6

            Console.WriteLine("Is this F? " + mazeData[5, 3]);


            Test test1 = new Test();

            Test test2 = new Test();

            test1.localInt = 4;
            test2.localInt = 7;

            Test.sharedInt = 52;

            Console.ReadLine();
        }
    }
}
