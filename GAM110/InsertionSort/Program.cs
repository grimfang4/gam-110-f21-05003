﻿using System;

namespace InsertionSort
{
    class Program
    {
        static void InsertionSort(int[] values)
        {
            int sortedLength = 1;
            while(sortedLength != values.Length)
            {
                int swapPosition = sortedLength;
                while(swapPosition != 0 && values[swapPosition] < values[swapPosition - 1])
                {
                    // Do swap
                    int swapper = values[swapPosition];
                    values[swapPosition] = values[swapPosition - 1];
                    values[swapPosition - 1] = swapper;

                    swapPosition--;
                }

                sortedLength++;
            }
        }

        static void PrintArray(int[] values)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                Console.Write(values[i] + " ");
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int[] values = new int[10];

            Random rng = new Random();
            for(int i = 0; i < values.Length; ++i)
            {
                values[i] = rng.Next(1, 21);
            }

            PrintArray(values);

            InsertionSort(values);

            PrintArray(values);

            Console.ReadLine();
        }
    }
}
