﻿using System;

class Program
{
    static void Main(string[] args)
    {
        bool done = false;
        while (!done)
        {
            bool goodInput = false;
            while (!goodInput)
            {
                Console.Clear();
                Console.WriteLine("What do you want to do?");
                Console.WriteLine("1) Attack");
                Console.WriteLine("2) Run\n3) Defend\n4) Quit");

                string input = Console.ReadLine();

                int inputInteger;

                if (int.TryParse(input, out inputInteger))
                {
                    goodInput = true;

                    if (inputInteger == 1)
                    {
                        // do attack
                    }
                    else if (inputInteger == 2)
                    {
                        // do run
                    }
                    else if (inputInteger == 3)
                    {
                        // do defend
                    }
                    else if (inputInteger == 4)
                    {
                        done = true;
                    }
                    else
                    {
                        // Invalid option!
                        Console.WriteLine("That is not an option.  Try again.  Press Enter to continue...");
                        goodInput = false;
                        Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("That is not even a number!  Press Enter to continue...");
                    goodInput = false;
                    Console.ReadLine();
                }
            }
        }



    }
}
