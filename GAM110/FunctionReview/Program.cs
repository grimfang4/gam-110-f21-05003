﻿using System;

namespace FunctionReview
{
    class Program
    {
        static int GetNextOdd(int n)
        {
            if (n % 2 == 0)
                return n + 1;
            return n + 2;
        }

        // Cooper's function
        /*static int GetNextOdd(int n)
        {
            if(n%2 == 0)
            {
                return n + 1;
            }
            else
            {
                return n + 2;
            }
        }*/

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int q = 17;
            int g = GetNextOdd(4);

            Console.WriteLine("Next odd after 4: " + g);

            if (IsReversedString("abcdef", "fedcba"))
                Console.WriteLine("1) Those are reverses!");
            else 
                Console.WriteLine("1) Those are NOT reverses!");

            if (IsReversedString("racecar", "not a racecar"))
                Console.WriteLine("2) Those are reverses!");
            else 
                Console.WriteLine("2) Those are NOT reverses!");

            Console.ReadLine();
        }
    }
}
