﻿using System;

class Program
{
    static void Main(string[] args)
    {
        //Intro message
        Console.WriteLine("You fool, you've fallen into my number trap and can no longer escape.\n You must guess the number that I am thinking in order to escape.");

        //Range of number 1-100
        Console.WriteLine("\nYou'll need to guess a number between 1 and 100");

        Random rng;
        rng = new Random();

        // (0, 10) Range - Exclusive
        // (0, 10] - Exclusive of lower bound, inclusive of upper bound
        // [0, 10] - Inclusive

        //Declare number to be guessed
        int numberToBeGuessed = rng.Next(1, 101);

        //declare that variable
        int guessedNumber = -1;

        //loop loop loop loop
        while (numberToBeGuessed != guessedNumber)
        {
            //some sort of message
            Console.Write("Guess number: ");

            //Read it!
            string typedResult;
            typedResult = Console.ReadLine();

            guessedNumber = int.Parse(typedResult);

            //tell if wrong or right
            //if statemetn!
            if (guessedNumber < numberToBeGuessed)
            {
                //tell them
                Console.WriteLine("HIGHER!");

                //another if statement!
            }
            else if (guessedNumber > numberToBeGuessed)
            {
                //tell them
                Console.WriteLine("LOWER!");

                //at some point they're gonna get it
            }
        }
        //message after the loop
        Console.WriteLine("\n\n YOU DID IT! ");
        Console.ReadLine();
    }
}
/*
 * keep track of number (int)
 * Random number generation? -we'll see
 * "Closer" or "Further" / "Higher" or "Lower"
 * actually 101 (rude)
 * some sorta parse
 * Comparison :: Condition (If statement)
 * Some sort of message when win
 *  IF equal then win 
 * Music?
 * Read Line
 * KEEP GUESSING : loops
 */

/*
 * Intro:
 *  I'm thinkin o
 */