﻿using System;

//Given a string, replace single spaces after a period with two spaces and break
//lines after every two sentences.

//Go through like an array
// \n
namespace TimProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            //I'm going to assume a "sentence" will always end with a period.
            string toGoThrough = "One day I went to the store. AT the store, there was a really pretty lady. Unfortunatelly I am terrible to with woman.";

            string placeholder = "";
            int count = 2;

            for( int i = 0; i< toGoThrough.Length; i++)
            {
                if(count == 0)
                {
                    count = 2;
                }
                if(toGoThrough[i] == '.')
                {
                    count--;
                    if(count == 0)
                    {
                        placeholder += "\n";
                    }
                    else
                    {
                        placeholder += " ";
                    }
                }
                else
                {
                    placeholder += toGoThrough[i];
                }
            }
            Console.WriteLine(placeholder);
            Console.Read();

        }
    }
}
