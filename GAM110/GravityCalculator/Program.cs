﻿using System;

namespace GravityCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            double G = 6.67408e-11;

            Planet planetA = new Planet();
            planetA.mass = 5.97e24;
            // TODO: Set position
            
            Planet planetB = new Planet();
            planetB.mass = 5.97e24;
            // TODO: Set position


            // TODO: Calculate force due to gravity

            // Print out in scientific notation
            // Shortened:
            string message1 = string.Format("{0:g}", planetA.mass);
            // Forced to scientific notation:
            string message2 = string.Format("{0:e}", planetA.mass);
            Console.WriteLine("SciNot: " + message1 + " vs. " + message2);
            //System.Environment.Exit(0);

            Console.ReadLine();
        }
    }
}
