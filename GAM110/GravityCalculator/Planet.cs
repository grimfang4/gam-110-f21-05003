﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GravityCalculator
{
    class Planet
    {
        public double mass;  // in kilograms
        public double x;
        public double y;
    }
}
