﻿using System;

namespace CarrotEconomy
{
    class Program
    {
        public static string[] descriptors = { "Strength", "Agility"};

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // TODO: Create Hero, create 3 stores, loop to choose store, inner loop for buying/selling

            //Carrot c = new Carrot();
            CarrotStore store1 = new CarrotStore();
            CarrotStore store2 = new CarrotStore();
            CarrotStore store3 = new CarrotStore();

            //CarrotStore[] stores = {new CarrotStore(), new CarrotStore(), new CarrotStore()};
            Hero hero = new Hero();

            bool gameOver = false;
            while(!gameOver)
            {
                Console.Clear();
                Console.WriteLine("Which store would you like to visit?");
                Console.WriteLine("1) " + store1.GetName());
                Console.WriteLine("2) " + store2.GetName());
                Console.WriteLine("3) " + store3.GetName());
                Console.WriteLine("4) Quit");

                string response = Console.ReadLine();
                int choice;
                if (!int.TryParse(response, out choice))
                    choice = -1;  // Player is naughty.

                switch(choice)
                {
                    case 1:
                        // TODO: Go shopping!
                        break;
                    case 2:
                        // TODO: Go shopping!
                        break;
                    case 3:
                        // TODO: Go shopping!
                        break;
                    case 4:
                        gameOver = true;
                        break;
                    default:
                        Console.WriteLine("That is not a valid choice!");
                        Console.WriteLine("Press Enter to continue...");
                        Console.ReadLine();
                        break;
                }
            }

        }
    }
}
