﻿using System;

namespace MazeGame
{
    class Program
    {
        static void ExploreUNICODE()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            //char c = (char)9689;
            char c = '\u25D9';
            Console.WriteLine(c + " is at value \\u" + string.Format("{0:X}", (int)c));

            bool done = false;
            while (!done)
            {
                for (int i = 0; i < 30; ++i)
                {
                    Console.Write((char)(c + (char)i));
                }

                // Get player input
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.RightArrow)
                    c += (char)30;
                else if (keyInfo.Key == ConsoleKey.LeftArrow)
                    c -= (char)30;
                else if (keyInfo.Key == ConsoleKey.Spacebar)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine();
                    for (int i = 0; i < 30; ++i)
                    {
                        char newC = (char)(c + (char)i);
                        Console.WriteLine(newC + " is at value \\u" + string.Format("{0:X}", (int)newC));
                    }
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.CursorVisible = false;

            //ExploreUNICODE();

            Console.WriteLine("Hello World!");

            Maze maze = new Maze();
            Player player = new Player(maze);

            bool done = false;
            while (!done)
            {
                // Redraw the maze
                Console.SetCursorPosition(0,0);
                maze.Draw();

                // Get player input
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.RightArrow)
                    player.MoveRight(maze);
                else if (keyInfo.Key == ConsoleKey.LeftArrow)
                    player.MoveUp(maze);
            }
        }
    }
}
