﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazeGame
{
    class Player
    {
        private int x;
        private int y;
        private char avatar = '@';
        private char currentTile;

        public Player(Maze maze)
        {
            x = -1;
            y = -1;

            // Find the Starting location and use that position to initialize the Player
            int startX = maze.GetWidth()/2;
            int startY = maze.GetHeight()/2;
            for(int i = 0; i < maze.GetHeight(); ++i)
            {
                for(int j = 0; j < maze.GetWidth(); ++j)
                {
                    if(maze.GetTile(j, i) == 'S')
                    {
                        startX = j;
                        startY = i;
                    }
                }
            }
            

            currentTile = maze.GetTile(startX, startY);
            MoveTo(startX, startY, maze);
        }

        public void MoveTo(int newX, int newY, Maze maze)
        {
            if (newX < 0 || newX >= maze.GetWidth() || newY < 0 || newY >= maze.GetHeight())
                return;

            char newCurrentTile = maze.GetTile(newX, newY);  // Get what is under new position
            if (newCurrentTile == '#')
                return;

            maze.SetTile(x, y, currentTile);  // Replace our last position
            currentTile = newCurrentTile;  // Store the new tile's contents

            // Actually move there
            x = newX;
            y = newY;
            maze.SetTile(x, y, avatar);
        }

        public void MoveUp(Maze maze)
        {
            MoveTo(x, y-1, maze);
        }

        public void MoveRight(Maze maze)
        {
            MoveTo(x+1, y, maze);
        }
    }
}
