﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ThisAndNull
{
    class Hero
    {
        public string name;
        public int gold;
        public int numCats;
        public int didgeridoos;


        public Hero()
        {
            name = "Default Name";
            gold = 10;
            numCats = 400;

            Program.lastHero = this;
        }

        public Hero(string name, int gold)
        {
            this.name = name;
            this.gold = gold;
            numCats = 400;

            Program.lastHero = this;
        }
        /*public Hero(string newName, int newGold)
        {
            name = newName;
            gold = newGold;
        }*/

        public void Save(string filename)
        {
            StreamWriter writer = new StreamWriter(filename);

            writer.WriteLine(name);
            writer.WriteLine(gold);
            writer.WriteLine(numCats);

            writer.Close();
        }

        public void Load(string filename)
        {
            StreamReader reader = new StreamReader(filename);

            name = reader.ReadLine();
            gold = int.Parse(reader.ReadLine());
            numCats = int.Parse(reader.ReadLine());
            didgeridoos = int.Parse(reader.ReadLine());

            while(reader.Peek() > -1)
            {
                string s = reader.ReadLine();
                Console.WriteLine("Extra data in save file: " + s);
            }

            reader.Close();
        }

    }
}
