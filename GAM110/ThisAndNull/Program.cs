﻿using System;

namespace ThisAndNull
{
    class Program
    {
        public static Hero lastHero = null;

        public static int maxNumHeroes = 4;
        public static Hero MakeHero()
        {
            if (maxNumHeroes <= 0)
                return null;

            maxNumHeroes--;
            return new Hero();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Hero myHero = new Hero("BAD DUDE NOT LOADED PROPERLY", -100);

            myHero.Load("save1.txt");
            Console.WriteLine("Last hero name: " + lastHero.name);
            //myHero.Save("save1.txt");


            while (true)
            {
                Console.WriteLine("Press Enter to create a Hero.");
                Console.ReadLine();

                Hero h1 = MakeHero();
                if(h1 == null)
                {
                    Console.WriteLine("No More Heroes!");
                    break;
                }
            }


            Console.ReadLine();

        }
    }
}
